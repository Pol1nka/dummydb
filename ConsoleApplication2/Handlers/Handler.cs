using System;
using System.Collections.Generic;
using System.IO;
using ConsoleApplication2.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;

namespace ConsoleApplication2.Handlers
{
    public class Handler<T>
    {

        public static List<T> GetData(string filename, string schema)
        {
            string result = GetJsonFromCsv(filename, "");
            if (!IsValid(result, schema))
            {
                Console.WriteLine("erorrr???");
                return null;
            }
            List<T> data = JsonConvert.DeserializeObject<List<T>>(result);
            return data;
        }
        public static bool IsValid(string json, string schema)
        {
            IList<string> messages = new List<string>();
            try
            {

                string schemaText = File.ReadAllText(schema);
                JsonTextReader reader = new JsonTextReader(new StringReader(json));

                JsonValidatingReader validatingReader = new JsonValidatingReader(reader);
                validatingReader.Schema = JsonSchema.Parse(schemaText);

                validatingReader.ValidationEventHandler += (o, a) => messages.Add(a.Message);
                JsonSerializer serializer = new JsonSerializer();
                var p = serializer.Deserialize<List<T>>(validatingReader);

                foreach (string s in messages)
                {
                    Console.WriteLine(s);
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                messages.Add(e.Message);
            }
            return messages.Count == 0;

        }
        
        public static string GetJsonFromCsv(string filename, string schema)
        {
            var csv = new List<string[]>(); 
            var lines= File.ReadAllLines(filename); 
            
            foreach (string line in lines)
                csv.Add(line.Split(';'));

            var properties = lines[0].Split(';');

            var listObjResult = new List<Dictionary<string, string>>();

            for (int i = 1; i < lines.Length; i++)
            {
                var objResult = new Dictionary<string, string>();
                for (int j = 0; j < properties.Length; j++)
                    objResult.Add(properties[j], csv[i][j]);

                listObjResult.Add(objResult);
            }

            var json = JsonConvert.SerializeObject(listObjResult);

            return json;
        }
    }
}