namespace ConsoleApplication2.Models
{
    public class Readers
    {
        private int ID;
        private string FullName;

        public Readers(int id, string fullName)
        {
            ID = id;
            FullName = fullName;
        }

        public override string ToString()
        {
            return $"{ID} {FullName}";
        }
    }
}