namespace ConsoleApplication2.Models
{
    public class Book
    {
        public int ID;
        private string Title;
        private string AuthorName;
        private int PublicationYear;
        private int ShelfIndex;
        private int BookcaseIndex;

        public Book(int id, string title, string authorName, int publicationYear, int shelfIndex, int bookcaseIndex)
        {
            ID = id;
            Title = title;
            AuthorName = authorName;
            PublicationYear = publicationYear;
            ShelfIndex = shelfIndex;
            BookcaseIndex = bookcaseIndex;
        }

        public override string ToString()
        {
            return $"{Title} [{AuthorName}]";
        }
    }
}