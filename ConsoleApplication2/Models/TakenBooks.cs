using System;
using System.Globalization;

namespace ConsoleApplication2.Models
{
    public class TakenBooks
    {
        public int BookID;
        public int ReaderID;
        private string GettingTime;
        private string ReturningTime;

        public DateTime GetTime;
        public DateTime ReturnTime;


        public TakenBooks(int bookId, int readerId, string gettingTime, string returningTime)
        {
            BookID = bookId;
            ReaderID = readerId;

            GettingTime = gettingTime;
            ReturningTime = returningTime;
            
            GetTime = DateTime.ParseExact(gettingTime, "yyyy.MM.dd", new CultureInfo("ru-RU"));
            ReturnTime = DateTime.ParseExact(returningTime, "yyyy.MM.dd", new CultureInfo("ru-RU"));
        }


        public override string ToString()
        {
            return $"{BookID} {ReaderID} {GettingTime} {ReturningTime}";
        }
    }
}