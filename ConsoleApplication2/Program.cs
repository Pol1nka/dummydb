﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConsoleApplication2.Handlers;
using ConsoleApplication2.Models;

namespace ConsoleApplication2
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            List<Book> books = Handler<Book>.GetData("../../Data/Books.csv", "../../Schemas/Books.json");
            List<Readers> readers = Handler<Readers>.GetData("../../Data/Readers.csv", "../../Schemas/Readers.json");
            List<TakenBooks> takenBooks = Handler<TakenBooks>.GetData("../../Data/TakenBooks.csv", "../../Schemas/TakenBooks.json");

            foreach (Book book in books)
            {
                bool checkIsBookOnHands = takenBooks.Any(x => x.BookID == book.ID && 
                                                                  x.GetTime < DateTime.Now
                                                                  && DateTime.Now < x.ReturnTime);
                if (checkIsBookOnHands)
                    Console.WriteLine($"{book} (На руках)");
                else
                    Console.WriteLine(book);
            }
        }
    }
}